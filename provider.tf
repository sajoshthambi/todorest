provider "google" {
  credentials = file("/var/lib/jenkins/sc.json")
  project     = "clever-case-261717"
  region      = "us-central1"
}